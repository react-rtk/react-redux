import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addTodo, deleteTodo } from "./redux/action";
import styled from "styled-components";

const TodoInput = styled.div`
  width: 200px;
  margin: 50px auto;
  display: flex;
  justify-content: space-between;
`;

const Todo = styled.div`
  width: 200px;
  margin: 20px auto;
  display: flex;
  justify-content: space-between;
`;

function App() {
  const [value, setValue] = useState("");

  const todo = useSelector((state) => state.todoReducer.todos);

  const dispatch = useDispatch();

  const handleChangeValue = (e) => {
    setValue(e.target.value);
  };

  return (
    <>
      <TodoInput>
        <input value={value} onChange={handleChangeValue}></input>
        <button
          onClick={() => {
            dispatch(addTodo(value));
          }}
        >
          add
        </button>
      </TodoInput>

      {todo.map((data) => {
        return (
          <Todo key={data.id}>
            {data.name}
            <button
              onClick={() => {
                dispatch(deleteTodo(data.id));
              }}
            >
              刪除
            </button>
          </Todo>
        );
      })}
    </>
  );
}

export default App;
