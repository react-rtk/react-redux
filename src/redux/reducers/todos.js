import { actionType } from "../actionType";

let todoId = 0;

const initialState = {
  todos: [],
};

export default function todoReducer(state = initialState, action) {
  switch (action.type) {
    case actionType.ADD_TODO: {
      return {
        todos: [...state.todos, {
             name: action.payload.name,id:todoId++ }
            ],
      };
    }
    case actionType.DELETE_TODO:
      return {
        todos: state.todos.filter((todo) => {
          return todo.id !== action.payload.id;
        }),
      };
    default:
      return state;
  }
}
