import { actionType } from "./actionType";

export function addTodo(name) {
    return {
        type:actionType.ADD_TODO,
        payload:{
            name
        }
    }
}

export function deleteTodo(id) {
    return {
        type:actionType.DELETE_TODO,
        payload:{
            id
        }
    }
}

